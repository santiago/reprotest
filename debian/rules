#! /usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

#export DH_VERBOSE = 1
export PYBUILD_NAME = reprotest

%:
	dh $@ --buildsystem=pybuild

, := ,
# It's hard to make sure disorderfs works on buildds, so drop it.
# The build user needs fuse access and this is not easily arranged.
#
# To be user-friendly the user_group variation defaults to ON but is a no-op.
# This causes tests to fail since they expect something to be captured. So ignore it here
#
# Also disable the num_cpus variation if $(nproc) = 1 as there is no variation
# in that case.
export REPROTEST_TEST_DONTVARY = fileordering,user_group,domain_host$(if $(shell nproc | grep --color=no -Fx 1),$(,)num_cpus,)

execute_before_dh_auto_configure:
	test $$(python3 setup.py --version) = $$(echo $(DEB_VERSION) | sed 's/[^0-9.].*//')

execute_after_dh_auto_build:
	$(MAKE) -C doc

override_dh_auto_test:
	VIRTUALENV_DOWNLOAD=no \
	http_proxy=http://127.0.9.1:9 \
	https_proxy=https://127.0.9.1:9 \
	TOX_TESTENV_PASSENV=PYTHONIOENCODING PYTHONIOENCODING=utf-8 \
	tox -r --sitepackages -- -s

# This is meant to be run by autopkgtest, and runs against the *installed*
# version of reprotest. To run it against the development version, give
# PYTHONPATH=$PWD. (Or just use override_dh_auto_test, above.)
autopkgtest-pytest:
# print the value, for debugging
	REPROTEST_TEST_DONTVARY="$(REPROTEST_TEST_DONTVARY)"
	pytest-3 $(if $(PYTEST_MARKEXPR),-m "$(PYTEST_MARKEXPR)",) $(PYTEST_ARGS)

execute_after_dh_auto_clean:
	$(MAKE) -C doc clean

override_dh_python3:
	dh_python3 -p reprotest --recommends-section=compare
